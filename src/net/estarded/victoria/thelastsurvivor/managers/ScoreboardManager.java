package net.estarded.victoria.thelastsurvivor.managers;

import net.estarded.victoria.thelastsurvivor.TheLastSurvivor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ScoreboardManager {

    public void setScoreboard(Player player){
        Scoreboard scoreboard = player.getScoreboard();
        Objective objective = scoreboard.getObjective("right"+ player.getName()) != null ? scoreboard.getObjective("right"+ player.getName()) : scoreboard.registerNewObjective("right"+ player.getName(), "dummy");

        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("§4The Last Survivor");
        objective.getScore("§8§m-----------------§a").setScore(6);
        objective.getScore("§eJoueur : §6"+ player.getName()).setScore(5);
        objective.getScore("§eHeure : §6"+ new SimpleDateFormat("HH:mm").format(new Date())).setScore(4);
        objective.getScore("§eKills : §6"+ TheLastSurvivor.getInstance().getKillsFileManager().getKills(player)).setScore(3);
        objective.getScore("§eZone : §6"+ TheLastSurvivor.getInstance().getCuboids().getOrDefault(player, "Aucune")).setScore(2);
        objective.getScore("§8§m-----------------§b").setScore(1);
        objective.getScore("§cDev : @Estarded").setScore(0);

        player.setScoreboard(scoreboard);
    }

    public void setNameTag(Player player){
        Scoreboard scoreboard = player.getScoreboard();
        Team team = scoreboard.getTeam("rank"+ player.getName()) != null ? scoreboard.getTeam("rank"+ player.getName()) : scoreboard.registerNewTeam("rank"+ player.getName());

        team.setPrefix(TheLastSurvivor.getInstance().getRanksFileManager().getRank(player).getPrefix());
        team.addEntry(player.getName());

        player.setScoreboard(scoreboard);
    }

}