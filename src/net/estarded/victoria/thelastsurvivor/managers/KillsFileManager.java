package net.estarded.victoria.thelastsurvivor.managers;

import net.estarded.victoria.thelastsurvivor.TheLastSurvivor;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

public class KillsFileManager {

    private File file = new File(TheLastSurvivor.getInstance().getDataFolder(), "kills.yml");

    public void createFile(){
        if(!file.exists()){
            TheLastSurvivor.getInstance().saveResource(file.getName(), false);
        }
    }

    public void setKills(Player player, int kills){
        TheLastSurvivor.getInstance().getKillsFile().set("KILLS."+ player.getUniqueId(), kills);
        try {
            TheLastSurvivor.getInstance().getKillsFile().save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean hasKills(Player player){
        if(TheLastSurvivor.getInstance().getRanksFile().getConfigurationSection("KILLS") != null){
            return TheLastSurvivor.getInstance().getRanksFile().getConfigurationSection("KILLS").getKeys(false).contains(player.getUniqueId().toString());
        }
        return false;
    }

    public int getKills(Player player){
        return TheLastSurvivor.getInstance().getKillsFile().getInt("KILLS."+ player.getUniqueId());
    }

}