package net.estarded.victoria.thelastsurvivor.managers;

import net.estarded.victoria.thelastsurvivor.TheLastSurvivor;
import net.estarded.victoria.thelastsurvivor.utils.CuboidUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public class CuboidsFileManager {

    private File file = new File(TheLastSurvivor.getInstance().getDataFolder(), "cuboids.yml");

    public void createFile(){
        if(!file.exists()){
            TheLastSurvivor.getInstance().saveResource(file.getName(), false);
        }
    }

    public void setCuboid(String name){
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION1.WORLD", "world");
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION1.X", 0);
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION1.Y", 0);
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION1.Z", 0);
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION2.WORLD", "world");
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION2.X", 0);
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION2.Y", 0);
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION2.Z", 0);
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".BUILD", 0);
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".PVP", 0);
        try {
            TheLastSurvivor.getInstance().getCuboidsFile().save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setLocation1Cuboid(String name, Location location){
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION1.WORLD", location.getWorld().getName());
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION1.X", location.getX());
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION1.Y", location.getY());
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION1.Z", location.getZ());
        try {
            TheLastSurvivor.getInstance().getCuboidsFile().save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setLocation2Cuboid(String name, Location location){
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION2.WORLD", location.getWorld().getName());
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION2.X", location.getX());
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION2.Y", location.getY());
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".LOCATION2.Z", location.getZ());
        try {
            TheLastSurvivor.getInstance().getCuboidsFile().save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setBuildCuboid(String name, boolean state){
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".BUILD", state);
        try {
            TheLastSurvivor.getInstance().getCuboidsFile().save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setPvPCuboid(String name, boolean state){
        TheLastSurvivor.getInstance().getCuboidsFile().set("CUBOIDS."+ name +".PVP", state);
        try {
            TheLastSurvivor.getInstance().getCuboidsFile().save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean containsCuboid(String name){
        if(TheLastSurvivor.getInstance().getCuboidsFile().getConfigurationSection("CUBOIDS") != null){
            return TheLastSurvivor.getInstance().getCuboidsFile().getConfigurationSection("CUBOIDS").getKeys(false).contains(name);
        }
        return false;
    }

    public Set<String> getCuboids(){
        if(TheLastSurvivor.getInstance().getCuboidsFile().getConfigurationSection("CUBOIDS") != null){
            return TheLastSurvivor.getInstance().getCuboidsFile().getConfigurationSection("CUBOIDS").getKeys(false);
        }
        return null;
    }

    public CuboidUtil getCuboid(String name){
        String world1 = TheLastSurvivor.getInstance().getCuboidsFile().getString("CUBOIDS."+ name +".LOCATION1.WORLD");
        int x1 = TheLastSurvivor.getInstance().getCuboidsFile().getInt("CUBOIDS."+ name +".LOCATION1.X");
        int y1 = TheLastSurvivor.getInstance().getCuboidsFile().getInt("CUBOIDS."+ name +".LOCATION1.Y");
        int z1 = TheLastSurvivor.getInstance().getCuboidsFile().getInt("CUBOIDS."+ name +".LOCATION1.Z");
        String world2 = TheLastSurvivor.getInstance().getCuboidsFile().getString("CUBOIDS."+ name +".LOCATION2.WORLD");
        int x2 = TheLastSurvivor.getInstance().getCuboidsFile().getInt("CUBOIDS."+ name +".LOCATION2.X");
        int y2 = TheLastSurvivor.getInstance().getCuboidsFile().getInt("CUBOIDS."+ name +".LOCATION2.Y");
        int z2 = TheLastSurvivor.getInstance().getCuboidsFile().getInt("CUBOIDS."+ name +".LOCATION2.Z");
        return new CuboidUtil(new Location(Bukkit.getWorld(world1), x1, y1, z1), new Location(Bukkit.getWorld(world2), x2, y2, z2));
    }

    public boolean getBuildCuboid(String name){
        return TheLastSurvivor.getInstance().getCuboidsFile().getBoolean("CUBOIDS."+ name +".BUILD");
    }

    public boolean getPvPCuboid(String name){
        return TheLastSurvivor.getInstance().getCuboidsFile().getBoolean("CUBOIDS."+ name +".PVP");
    }

}