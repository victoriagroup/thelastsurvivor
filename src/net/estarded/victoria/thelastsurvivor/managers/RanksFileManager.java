package net.estarded.victoria.thelastsurvivor.managers;

import net.estarded.victoria.thelastsurvivor.TheLastSurvivor;
import net.estarded.victoria.thelastsurvivor.units.RankUnit;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

public class RanksFileManager {

    private File file = new File(TheLastSurvivor.getInstance().getDataFolder(), "ranks.yml");

    public void createFile(){
        if(!file.exists()){
            TheLastSurvivor.getInstance().saveResource(file.getName(), false);
        }
    }

    public void setRank(Player player, RankUnit rank){
        TheLastSurvivor.getInstance().getRanksFile().set("RANKS."+ player.getUniqueId(), rank.getPower());
        try {
            TheLastSurvivor.getInstance().getRanksFile().save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean hasRank(Player player){
        if(TheLastSurvivor.getInstance().getRanksFile().getConfigurationSection("RANKS") != null){
            return TheLastSurvivor.getInstance().getRanksFile().getConfigurationSection("RANKS").getKeys(false).contains(player.getUniqueId().toString());
        }
        return false;
    }

    public RankUnit getRank(Player player){
        return RankUnit.getFromPower(TheLastSurvivor.getInstance().getRanksFile().getInt("RANKS."+ player.getUniqueId()));
    }

}