package net.estarded.victoria.thelastsurvivor.event;

import net.estarded.victoria.thelastsurvivor.TheLastSurvivor;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class BlockBreakEvent implements Listener {

    @EventHandler
    public void onBlockBreak(org.bukkit.event.block.BlockBreakEvent event){
        Block block = event.getBlock();

        if(TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboids() != null){
            TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboids().stream().filter(name -> TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboid(name) != null && TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboid(name).contains(block.getLocation())).forEach(name -> event.setCancelled(!TheLastSurvivor.getInstance().getCuboidsFileManager().getBuildCuboid(name)));
        }
    }

}