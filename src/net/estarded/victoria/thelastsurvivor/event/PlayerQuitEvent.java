package net.estarded.victoria.thelastsurvivor.event;

import net.estarded.victoria.thelastsurvivor.TheLastSurvivor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerQuitEvent implements Listener {

    @EventHandler
    public void onPlayerQuit(org.bukkit.event.player.PlayerQuitEvent event){
        Player player = event.getPlayer();

        event.setQuitMessage(TheLastSurvivor.getInstance().getPrefix() +"§6"+ player.getName() +" §evient de quitter le serveur !");
    }

}