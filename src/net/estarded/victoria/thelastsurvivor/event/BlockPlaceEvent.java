package net.estarded.victoria.thelastsurvivor.event;

import net.estarded.victoria.thelastsurvivor.TheLastSurvivor;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class BlockPlaceEvent implements Listener {

    @EventHandler
    public void onBlockPlace(org.bukkit.event.block.BlockPlaceEvent event){
        Block block = event.getBlock();

        if(TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboids() != null){
            TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboids().stream().filter(name -> TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboid(name) != null && TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboid(name).contains(block.getLocation())).forEach(name -> event.setCancelled(!TheLastSurvivor.getInstance().getCuboidsFileManager().getBuildCuboid(name)));
        }
    }

}