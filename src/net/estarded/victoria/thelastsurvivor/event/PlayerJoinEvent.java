package net.estarded.victoria.thelastsurvivor.event;

import net.estarded.victoria.thelastsurvivor.TheLastSurvivor;
import net.estarded.victoria.thelastsurvivor.timers.ScoreboardTimer;
import net.estarded.victoria.thelastsurvivor.units.RankUnit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerJoinEvent implements Listener {

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onPlayerJoin(org.bukkit.event.player.PlayerJoinEvent event){
        Player player = event.getPlayer();

        event.setJoinMessage(TheLastSurvivor.getInstance().getPrefix() +"§6"+ player.getName() +" §evient de rejoindre le serveur !");

        if(!TheLastSurvivor.getInstance().getRanksFileManager().hasRank(player)){
            TheLastSurvivor.getInstance().getRanksFileManager().setRank(player, RankUnit.JOUEUR);
        }

        if(!TheLastSurvivor.getInstance().getKillsFileManager().hasKills(player)){
            TheLastSurvivor.getInstance().getKillsFileManager().setKills(player, 0);
        }

        TheLastSurvivor.getInstance().getScoreboardManager().setScoreboard(player);
        TheLastSurvivor.getInstance().getScoreboardManager().setNameTag(player);
        new ScoreboardTimer().runTaskTimer(TheLastSurvivor.getInstance(), 20, 20);
    }

}