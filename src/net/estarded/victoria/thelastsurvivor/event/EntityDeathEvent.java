package net.estarded.victoria.thelastsurvivor.event;

import net.estarded.victoria.thelastsurvivor.TheLastSurvivor;
import net.estarded.victoria.thelastsurvivor.utils.ItemBuilderUtil;
import org.bukkit.Material;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.Random;

public class EntityDeathEvent implements Listener {

    @EventHandler
    public void onEntityDeath(org.bukkit.event.entity.EntityDeathEvent event){
        Entity entity = event.getEntity();

        if(entity instanceof Player){
            Player player = (Player) entity;
            TheLastSurvivor.getInstance().getKillsFileManager().setKills(player.getKiller(), TheLastSurvivor.getInstance().getKillsFileManager().getKills(player.getKiller()) + 1);
        }

        if(entity instanceof Zombie){
            event.getDrops().clear();
            int random = new Random().nextInt(10);
            if(random == 1){
                event.getDrops().add(new ItemBuilderUtil(Material.GOLD_NUGGET, 1).setName("§cPièce").toItemStack());
            }
        }

        if(entity instanceof Cow){
            event.getDrops().clear();
            int random = new Random().nextInt(20);
            if(random == 1){
                event.getDrops().add(new ItemBuilderUtil(Material.COOKED_BEEF, 1).toItemStack());
            }
        }
    }

}