package net.estarded.victoria.thelastsurvivor.event;

import net.estarded.victoria.thelastsurvivor.TheLastSurvivor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerMoveEvent implements Listener {

    @EventHandler
    public void onPlayerMove(org.bukkit.event.player.PlayerMoveEvent event){
        Player player = event.getPlayer();

        if(TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboids() != null){
            TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboids().stream().filter(name -> TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboid(name) != null && TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboid(name).contains(player.getLocation())).forEach(name -> {
                if(TheLastSurvivor.getInstance().getCuboids().containsKey(player)){
                    if(!TheLastSurvivor.getInstance().getCuboids().containsValue(name)){
                        TheLastSurvivor.getInstance().getCuboids().replace(player, name);
                        player.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§eVous entrez dans la zone : §6"+ name);
                    }
                }else{
                    TheLastSurvivor.getInstance().getCuboids().put(player, name);
                    player.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§eVous entrez dans la zone : §6"+ name);
                }
            });

            if(TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboids().stream().noneMatch(name -> TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboid(name).contains(player.getLocation()))){
                TheLastSurvivor.getInstance().getCuboids().remove(player);
            }
        }
    }

}