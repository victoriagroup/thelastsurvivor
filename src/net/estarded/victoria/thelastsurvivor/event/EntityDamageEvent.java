package net.estarded.victoria.thelastsurvivor.event;

import net.estarded.victoria.thelastsurvivor.TheLastSurvivor;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class EntityDamageEvent implements Listener {

    @EventHandler
    public void onEntityDamage(org.bukkit.event.entity.EntityDamageEvent event){
        Entity entity = event.getEntity();

        if(TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboids() != null){
            TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboids().stream().filter(name -> TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboid(name) != null && TheLastSurvivor.getInstance().getCuboidsFileManager().getCuboid(name).contains(entity.getLocation())).forEach(name -> event.setCancelled(!TheLastSurvivor.getInstance().getCuboidsFileManager().getPvPCuboid(name)));
        }
    }

}