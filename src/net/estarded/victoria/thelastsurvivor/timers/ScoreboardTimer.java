package net.estarded.victoria.thelastsurvivor.timers;

import net.estarded.victoria.thelastsurvivor.TheLastSurvivor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ScoreboardTimer extends BukkitRunnable {

    @Override
    public void run(){

        for(Player player : Bukkit.getOnlinePlayers()){
            player.getScoreboard().getObjective("right"+ player.getName()).getScoreboard().getEntries().stream().filter(entry -> !entry.contains(new SimpleDateFormat("HH:mm").format(new Date())) && entry.startsWith("§eHeure")).forEach(entry -> player.getScoreboard().getObjective("right"+ player.getName()).getScoreboard().resetScores(entry));
            player.getScoreboard().getObjective("right"+ player.getName()).getScore("§eHeure : §6"+ new SimpleDateFormat("HH:mm").format(new Date())).setScore(4);

            player.getScoreboard().getObjective("right"+ player.getName()).getScoreboard().getEntries().stream().filter(entry -> !entry.contains(String.valueOf(TheLastSurvivor.getInstance().getKillsFileManager().getKills(player))) && entry.startsWith("§eKills")).forEach(entry -> player.getScoreboard().getObjective("right"+ player.getName()).getScoreboard().resetScores(entry));
            player.getScoreboard().getObjective("right"+ player.getName()).getScore("§eKills : §6"+ TheLastSurvivor.getInstance().getKillsFileManager().getKills(player)).setScore(3);

            player.getScoreboard().getObjective("right"+ player.getName()).getScoreboard().getEntries().stream().filter(entry -> !entry.contains(TheLastSurvivor.getInstance().getCuboids().getOrDefault(player, "Aucune")) && entry.startsWith("§eZone")).forEach(entry -> player.getScoreboard().getObjective("right"+ player.getName()).getScoreboard().resetScores(entry));
            player.getScoreboard().getObjective("right"+ player.getName()).getScore("§eZone : §6"+ TheLastSurvivor.getInstance().getCuboids().getOrDefault(player, "Aucune")).setScore(2);
        }

    }

}