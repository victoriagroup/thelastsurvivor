package net.estarded.victoria.thelastsurvivor.commands;

import net.estarded.victoria.thelastsurvivor.TheLastSurvivor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Set;

public class CuboidCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){

        if(args.length == 2){

            switch(args[0].toLowerCase()){

                case "new":
                    String newName = args[1];

                    if(!TheLastSurvivor.getInstance().getCuboidsFileManager().containsCuboid(newName)){
                        TheLastSurvivor.getInstance().getCuboidsFileManager().setCuboid(newName);
                        sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§eVous avez ajouté la zone avec succès !");
                        return false;
                    }else{
                        sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cLa zone existe déjà !");
                        return false;
                    }

                default:
                    sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cErreur : /cuboid <new/setlocation/build/pvp> <1/2/on/off> <nom>");
                    return false;

            }

        }

        if(args.length == 3){

            switch(args[0].toLowerCase()){

                case "setlocation":
                    switch(args[1].toLowerCase()){

                        case "1":
                            if(sender instanceof Player){
                                Player player = (Player) sender;
                                String location1Name = args[2];

                                if(TheLastSurvivor.getInstance().getCuboidsFileManager().containsCuboid(location1Name)){
                                    TheLastSurvivor.getInstance().getCuboidsFileManager().setLocation1Cuboid(location1Name, player.getTargetBlock((Set<Material>) null, 100).getLocation());
                                    player.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§eVous avez défini la location 1 avec succès !");
                                    return false;
                                }else{
                                    player.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cLa zone n'existe pas !");
                                    return false;
                                }
                            }

                        case "2":
                            if(sender instanceof Player){
                                Player player = (Player) sender;
                                String location2Name = args[2];

                                if(TheLastSurvivor.getInstance().getCuboidsFileManager().containsCuboid(location2Name)){
                                    TheLastSurvivor.getInstance().getCuboidsFileManager().setLocation2Cuboid(location2Name, player.getTargetBlock((Set<Material>) null, 100).getLocation());
                                    player.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§eVous avez défini la location 2 avec succès !");
                                    return false;
                                }else{
                                    player.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cLa zone n'existe pas !");
                                    return false;
                                }
                            }

                        default:
                            sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cErreur : /cuboid <new/setlocation/build/pvp> <1/2/on/off> <nom>");
                            return false;

                    }

                case "build":
                    switch(args[1].toLowerCase()){

                        case "on":
                            String buildOnName = args[2];

                            if(TheLastSurvivor.getInstance().getCuboidsFileManager().containsCuboid(buildOnName)){
                                TheLastSurvivor.getInstance().getCuboidsFileManager().setBuildCuboid(buildOnName, true);
                                sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§eVous avez activé la construction dans la zone");
                                return false;
                            }else{
                                sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cLa zone n'existe pas !");
                                return false;
                            }

                        case "off":
                            String buildOffName = args[2];

                            if(TheLastSurvivor.getInstance().getCuboidsFileManager().containsCuboid(buildOffName)){
                                TheLastSurvivor.getInstance().getCuboidsFileManager().setBuildCuboid(buildOffName, true);
                                sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§eVous avez désactivé la construction dans la zone");
                                return false;
                            }else{
                                sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cLa zone n'existe pas !");
                                return false;
                            }

                        default:
                            sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cErreur : /cuboid <new/setlocation/build/pvp> <1/2/on/off> <nom>");
                            return false;

                    }

                case "pvp":
                    switch(args[1].toLowerCase()){

                        case "on":
                            String pvpOnName = args[2];

                            if(TheLastSurvivor.getInstance().getCuboidsFileManager().containsCuboid(pvpOnName)){
                                TheLastSurvivor.getInstance().getCuboidsFileManager().setPvPCuboid(pvpOnName, true);
                                sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§eVous avez activé le PvP dans la zone");
                                return false;
                            }else{
                                sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cLa zone n'existe pas !");
                                return false;
                            }

                        case "off":
                            String pvpOffName = args[2];

                            if(TheLastSurvivor.getInstance().getCuboidsFileManager().containsCuboid(pvpOffName)){
                                TheLastSurvivor.getInstance().getCuboidsFileManager().setPvPCuboid(pvpOffName, true);
                                sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§eVous avez désactivé le PvP dans la zone");
                                return false;
                            }else{
                                sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cLa zone n'existe pas !");
                                return false;
                            }

                        default:
                            sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cErreur : /cuboid <new/setlocation/build/pvp> <1/2/on/off> <nom>");
                            return false;

                    }

                default:
                    sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cErreur : /cuboid <new/setlocation/build/pvp> <1/2/on/off> <nom>");
                    return false;

            }

        }

        sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cErreur : /cuboid <new/setlocation/build/pvp> <1/2/on/off> <nom>");
        return false;

    }

}