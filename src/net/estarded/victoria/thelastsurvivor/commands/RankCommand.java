package net.estarded.victoria.thelastsurvivor.commands;

import net.estarded.victoria.thelastsurvivor.TheLastSurvivor;
import net.estarded.victoria.thelastsurvivor.units.RankUnit;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RankCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){

        if(args.length == 3){

            switch(args[0].toLowerCase()){

                case "set":
                    RankUnit rank = RankUnit.getFromName(args[1]);
                    Player target = Bukkit.getPlayer(args[2]);

                    if(rank != null){
                        if(target != null){
                            TheLastSurvivor.getInstance().getRanksFileManager().setRank(target, rank);
                            sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§eVous avez défini le grade avec succès !");
                            return false;
                        }else{
                            sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cLe joueur n'est pas connecté !");
                            return false;
                        }
                    }else{
                        sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cLe grade n'existe pas !");
                        return false;
                    }

                default:
                    sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cErreur : /rank <set> <grade> <joueur>");
                    return false;

            }

        }

        sender.sendMessage(TheLastSurvivor.getInstance().getPrefix() +"§cErreur : /rank <set> <grade> <joueur>");
        return false;

    }

}