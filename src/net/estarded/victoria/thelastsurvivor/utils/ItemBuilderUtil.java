package net.estarded.victoria.thelastsurvivor.utils;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;

public class ItemBuilderUtil {

    private ItemStack is;

    public ItemBuilderUtil(Material material){
        this(material, 1);
    }

    public ItemBuilderUtil(ItemStack is){
        this.is = is;
    }

    public ItemBuilderUtil(Material material, int amount){
        is = new ItemStack(material, amount);
    }

    public ItemBuilderUtil(Material material, int amount, short meta){
        is = new ItemStack(material, amount, meta);
    }

    public ItemBuilderUtil clone(){
        return new ItemBuilderUtil(is);
    }

    public ItemBuilderUtil setDurability(short durability){
        is.setDurability(durability);
        return this;
    }

    public ItemBuilderUtil setName(String name){
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        is.setItemMeta(im);
        return this;
    }

    public ItemBuilderUtil addUnsafeEnchantment(Enchantment enchantment, int level){
        is.addUnsafeEnchantment(enchantment, level);
        return this;
    }

    public ItemBuilderUtil removeEnchantment(Enchantment enchantment){
        is.removeEnchantment(enchantment);
        return this;
    }

    public ItemBuilderUtil setSkullOwner(String owner){
        try {
            SkullMeta im = (SkullMeta) is.getItemMeta();
            im.setOwner(owner);
            is.setItemMeta(im);
        } catch (ClassCastException expected) {
            expected.printStackTrace();
        }
        return this;
    }

    public ItemBuilderUtil addEnchant(Enchantment enchantment, int level){
        ItemMeta im = is.getItemMeta();
        im.addEnchant(enchantment, level, true);
        is.setItemMeta(im);
        return this;
    }

    public ItemBuilderUtil setInfinityDurability(){
        is.setDurability(Short.MAX_VALUE);
        return this;
    }

    public ItemBuilderUtil setLore(String... lore){
        ItemMeta im = is.getItemMeta();
        im.setLore(Arrays.asList(lore));
        is.setItemMeta(im);
        return this;
    }

    public ItemBuilderUtil setLeatherArmorColor(Color color){
        try {
            LeatherArmorMeta im = (LeatherArmorMeta) is.getItemMeta();
            im.setColor(color);
            is.setItemMeta(im);
        } catch (ClassCastException expected) {
            expected.printStackTrace();
        }
        return this;
    }

    public ItemStack toItemStack(){
        return is;
    }

}