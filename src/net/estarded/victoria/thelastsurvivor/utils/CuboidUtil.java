package net.estarded.victoria.thelastsurvivor.utils;

import org.bukkit.*;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CuboidUtil {

    private String worldName;
    private int x1;
    private int y1;
    private int z1;
    private int x2;
    private int y2;
    private int z2;

    public CuboidUtil(Location location1, Location location2){
        if(!location1.getWorld().equals(location2.getWorld())){
            throw new IllegalArgumentException("Les locations ne sont pas dans le même monde !");
        }
        worldName = location1.getWorld().getName();
        x1 = Math.min(location1.getBlockX(), location2.getBlockX());
        y1 = Math.min(location1.getBlockY(), location2.getBlockY());
        z1 = Math.min(location1.getBlockZ(), location2.getBlockZ());
        x2 = Math.max(location1.getBlockX(), location2.getBlockX());
        y2 = Math.max(location1.getBlockY(), location2.getBlockY());
        z2 = Math.max(location1.getBlockZ(), location2.getBlockZ());
    }

    public CuboidUtil(Location location1){
        this(location1, location1);
    }

    public CuboidUtil(CuboidUtil other){
        this(other.getWorld().getName(), other.x1, other.y1, other.z1, other.x2, other.y2, other.z2);
    }

    public CuboidUtil(World world, int x1, int y1, int z1, int x2, int y2, int z2){
        this.worldName = world.getName();
        this.x1 = Math.min(x1, x2);
        this.x2 = Math.max(x1, x2);
        this.y1 = Math.min(y1, y2);
        this.y2 = Math.max(y1, y2);
        this.z1 = Math.min(z1, z2);
        this.z2 = Math.max(z1, z2);
    }

    private CuboidUtil(String worldName, int x1, int y1, int z1, int x2, int y2, int z2){
        this.worldName = worldName;
        this.x1 = Math.min(x1, x2);
        this.x2 = Math.max(x1, x2);
        this.y1 = Math.min(y1, y2);
        this.y2 = Math.max(y1, y2);
        this.z1 = Math.min(z1, z2);
        this.z2 = Math.max(z1, z2);
    }

    public CuboidUtil(Map<String, Object> map){
        worldName = (String) map.get("worldName");
        x1 = (Integer) map.get("x1");
        x2 = (Integer) map.get("x2");
        y1 = (Integer) map.get("y1");
        y2 = (Integer) map.get("y2");
        z1 = (Integer) map.get("z1");
        z2 = (Integer) map.get("z2");
    }

    public Location getLowerNE(){
        return new Location(getWorld(), x1, y1, z1);
    }

    public Location getUpperSW(){
        return new Location(getWorld(), x2, y2, z2);
    }

    public Location getCenter(){
        int x1 = getUpperX() + 1;
        int y1 = getUpperY() + 1;
        int z1 = getUpperZ() + 1;
        return new Location(getWorld(), getLowerX() + (x1 - getLowerX()) / 2.0, getLowerY() + (y1 - getLowerY()) / 2.0, getLowerZ() + (z1 - getLowerZ()) / 2.0);
    }

    public World getWorld(){
        World world = Bukkit.getWorld(worldName);
        if(world == null){
            throw new IllegalStateException("Le monde "+ worldName +" n'est pas chargé !");
        }
        return world;
    }

    public int getSizeX(){
        return (x2 - x1) + 1;
    }

    public int getSizeY(){
        return (y2 - y1) + 1;
    }

    public int getSizeZ(){
        return (z2 - z1) + 1;
    }

    public int getLowerX(){
        return x1;
    }

    public int getLowerY(){
        return y1;
    }

    public int getLowerZ(){
        return z1;
    }

    public int getUpperX(){
        return x2;
    }

    public int getUpperY(){
        return y2;
    }

    public int getUpperZ(){
        return z2;
    }

    public Block[] corners(){
        Block[] res = new Block[8];
        World w = getWorld();
        res[0] = w.getBlockAt(x1, y1, z1);
        res[1] = w.getBlockAt(x1, y1, z2);
        res[2] = w.getBlockAt(x1, y2, z1);
        res[3] = w.getBlockAt(x1, y2, z2);
        res[4] = w.getBlockAt(x2, y1, z1);
        res[5] = w.getBlockAt(x2, y1, z2);
        res[6] = w.getBlockAt(x2, y2, z1);
        res[7] = w.getBlockAt(x2, y2, z2);
        return res;
    }

    public boolean contains(int x, int y, int z){
        return x >= x1 && x <= x2 && y >= y1 && y <= y2 && z >= z1 && z <= z2;
    }

    public boolean contains(Block block){
        return contains(block.getLocation());
    }

    public boolean contains(Location loaction){
        return worldName.equals(loaction.getWorld().getName()) && contains(loaction.getBlockX(), loaction.getBlockY(), loaction.getBlockZ());
    }

    public int volume(){
        return getSizeX() * getSizeY() * getSizeZ();
    }

    public CuboidUtil getBoundingCuboid(CuboidUtil other){
        if(other == null){
            return this;
        }

        int xMin = Math.min(getLowerX(), other.getLowerX());
        int yMin = Math.min(getLowerY(), other.getLowerY());
        int zMin = Math.min(getLowerZ(), other.getLowerZ());
        int xMax = Math.max(getUpperX(), other.getUpperX());
        int yMax = Math.max(getUpperY(), other.getUpperY());
        int zMax = Math.max(getUpperZ(), other.getUpperZ());

        return new CuboidUtil(worldName, xMin, yMin, zMin, xMax, yMax, zMax);
    }

    public Block getRelativeBlock(int x, int y, int z){
        return getWorld().getBlockAt(x1 + x, y1 + y, z1 + z);
    }

    public Block getRelativeBlock(World world, int x, int y, int z){
        return world.getBlockAt(x1 + x, y1 + y, z1 + z);
    }

    public List<Chunk> getChunks(){
        List<Chunk> res = new ArrayList<>();
        World w = getWorld();
        int x1 = getLowerX() & ~0xf; int x2 = getUpperX() & ~0xf;
        int z1 = getLowerZ() & ~0xf; int z2 = getUpperZ() & ~0xf;

        for(int x = x1; x <= x2; x += 16){
            for(int z = z1; z <= z2; z += 16){
                res.add(w.getChunkAt(x >> 4, z >> 4));
            }
        }

        return res;
    }

    @Override
    public String toString(){
        return "Cuboid : "+ worldName +","+ x1 +","+ y1 +","+ z1 +"=>"+ x2 +","+ y2 +","+ z2;
    }

}