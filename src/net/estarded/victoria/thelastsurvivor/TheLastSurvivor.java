package net.estarded.victoria.thelastsurvivor;

import net.estarded.victoria.thelastsurvivor.commands.CuboidCommand;
import net.estarded.victoria.thelastsurvivor.commands.RankCommand;
import net.estarded.victoria.thelastsurvivor.event.*;
import net.estarded.victoria.thelastsurvivor.managers.CuboidsFileManager;
import net.estarded.victoria.thelastsurvivor.managers.KillsFileManager;
import net.estarded.victoria.thelastsurvivor.managers.RanksFileManager;
import net.estarded.victoria.thelastsurvivor.managers.ScoreboardManager;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashMap;

public class TheLastSurvivor extends JavaPlugin {

    private static TheLastSurvivor instance;

    private HashMap<Player, String> cuboids;

    private CuboidsFileManager cuboidsFileManager;
    private RanksFileManager ranksFileManager;
    private KillsFileManager killsFileManager;
    private ScoreboardManager scoreboardManager;

    private YamlConfiguration cuboidsFile;
    private YamlConfiguration ranksFile;
    private YamlConfiguration killsFile;

    @Override
    public void onEnable(){
        super.onEnable();
        instance = this;
        cuboids = new HashMap<>();
        cuboidsFileManager = new CuboidsFileManager();
        ranksFileManager = new RanksFileManager();
        killsFileManager = new KillsFileManager();
        scoreboardManager = new ScoreboardManager();
        cuboidsFile = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "cuboids.yml"));
        ranksFile = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "ranks.yml"));
        killsFile = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "kills.yml"));
        getCuboidsFileManager().createFile();
        getRanksFileManager().createFile();
        getKillsFileManager().createFile();
        registerEvents();
        registerCommands();
    }

    @Override
    public void onDisable(){
        super.onDisable();
    }

    public static TheLastSurvivor getInstance(){
        return instance;
    }

    public HashMap<Player, String> getCuboids(){
        return cuboids;
    }

    public CuboidsFileManager getCuboidsFileManager(){
        return cuboidsFileManager;
    }

    public RanksFileManager getRanksFileManager(){
        return ranksFileManager;
    }

    public KillsFileManager getKillsFileManager(){
        return killsFileManager;
    }

    public ScoreboardManager getScoreboardManager(){
        return scoreboardManager;
    }

    public YamlConfiguration getCuboidsFile(){
        return cuboidsFile;
    }

    public YamlConfiguration getRanksFile(){
        return ranksFile;
    }

    public YamlConfiguration getKillsFile(){
        return killsFile;
    }

    public String getPrefix(){
        return "§8[§4The Last Survivor§8] §f";
    }

    private void registerEvents(){
        getServer().getPluginManager().registerEvents(new PlayerJoinEvent(), this);
        getServer().getPluginManager().registerEvents(new PlayerQuitEvent(), this);
        getServer().getPluginManager().registerEvents(new PlayerMoveEvent(), this);
        getServer().getPluginManager().registerEvents(new BlockPlaceEvent(), this);
        getServer().getPluginManager().registerEvents(new BlockBreakEvent(), this);
        getServer().getPluginManager().registerEvents(new EntityDamageEvent(), this);
        getServer().getPluginManager().registerEvents(new EntityDeathEvent(), this);
    }

    private void registerCommands(){
        getCommand("cuboid").setExecutor(new CuboidCommand());
        getCommand("rank").setExecutor(new RankCommand());
    }

}