package net.estarded.victoria.thelastsurvivor.units;

import java.util.HashMap;
import java.util.Map;

public enum RankUnit {

    JOUEUR(20, "Joueur", "§7[Joueur] ", ""),
    ADMIN(10, "Admin", "§c[Admin] ", "");

    private int power;
    private String name;
    private String prefix;
    private String suffix;

    private static Map<Integer, RankUnit> fromPower = new HashMap<>();
    private static Map<String, RankUnit> fromName = new HashMap<>();

    RankUnit(int power, String name, String prefix, String suffix){
        this.power = power;
        this.name = name;
        this.prefix = prefix;
        this.suffix = suffix;
    }

    static {
        for(RankUnit rank : values()){
            fromPower.put(rank.power, rank);
            fromName.put(rank.name, rank);
        }
    }

    public static RankUnit getFromPower(int power){
        return fromPower.get(power);
    }

    public static RankUnit getFromName(String name){
        for(String names : fromName.keySet()){
            if(name.equalsIgnoreCase(names)){
                return fromName.get(names);
            }
        }
        return null;
    }

    public int getPower(){
        return power;
    }

    public String getName(){
        return name;
    }

    public String getPrefix(){
        return prefix;
    }

    public String getSuffix(){
        return suffix;
    }

}